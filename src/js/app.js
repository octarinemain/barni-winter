$(document).ready(function () {

    // Home Slider
    var homeSwipper = new Swiper('.instruction', {
        slidesPerView: 3,
        allowTouchMove: false,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        breakpoints: {
            767: {
                slidesPerView: 1,
                allowTouchMove: true,
                spaceBetween: 80
            }
        }
    });

    // Rules Swiper
    var productSwiper = new Swiper('.white-block-slider', {
        slidesPerView: 3,
        spaceBetween: 25,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        breakpoints: {
            768: {
                slidesPerView: 2,
                spaceBetween: 40,
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 40,
            }
        }
    });

    // Cookies
    function cookies() {
        let cookies = $('.cookies');
        let data = sessionStorage.getItem('cookie');
        if (data === null) {
            cookies.addClass('js-show');
        }

        $('.cookies .btn').on('click', function (e) {
            e.preventDefault();
            cookies.removeClass('js-show');
            sessionStorage.setItem('cookie', 'ok');
        });
    }

    cookies();

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });

        closePopup();
    }


    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function (e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }

    popUp();

    // Input Password
    $('.js-pass').on('click', function () {
        var input = $(this).closest('div').find('input');
        if (input.attr('type') === 'password') {
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    });

    // Select
    $('select').select2();

    // Jquery Validate
    $('form').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                surname: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                secondname: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                city: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                password: {
                    required: true
                },
                personalAgreement: {
                    required: true
                },
                rules: {
                    required: true
                },
                captcha: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                }
            }
        });

        jQuery.validator.addMethod("phone", function (value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });

    // Btn-Disable
    function btnDis() {
        var popup = $('.popup');

        popup.each(function () {
            var validate = $(this).find('input', 'textarea');
            var validateForm = $(this).find('form');
            var btn = $(this).find('.btn');

            validate.on('blur keyup click', function () {
                var checked = $('input:checked').length;
                if (validateForm.valid() && checked) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                }
                if (validateForm.valid()) {
                    btn.prop('disabled', false).removeClass('btn_disable');
                } else {
                    btn.prop('disabled', 'disabled').addClass('btn_disable');
                }
            });
        });
    };

    btnDis();

    // Scroll
    $('.winners__table').scrollbar();
    $('.personal-wrap__table').scrollbar();

    // Masked Phone
    $("input[type='tel']").mask("+7(999)999-99-99");

    // Burger Menu
    function toggleMobMenu() {
        $('.header__burger').on('click', function () {
            $(this).toggleClass('active');
            $('.nav').toggleClass('active');
            $('.bg-fixed').toggleClass('bg-fixed_active');

            $(document).on('click', function (e) {
                var div = $(".nav, .header__burger");
                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.header__burger').removeClass('active');
                    $('.nav').removeClass('active');
                    $('.bg-fixed').removeClass('bg-fixed_active');
                }
            });
        });
    }
    toggleMobMenu();

    // Paralax
    function parallaxDesk() {
        $(window).on('scroll', function () {
            var scrolled = $(window).scrollTop();
            $('.container-img_first').css('top', (0 + (scrolled * .05)) + 'px');
            $('.container-img_second').css('top', (0 + (scrolled * .200)) + 'px');
            $('.container-img_third').css('top', (0 + (scrolled * .600)) + 'px');
        });
    };
    parallaxDesk();

    // Tabs
    function tabSelect() {
        $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
            $(this)
                .addClass('active').siblings().removeClass('active')
                .closest('div.tabs-wrapper').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
        });
    }
    tabSelect();

    //ImagePreview {
    function previewFile() {
        var preview = document.querySelector('.upload-file-img');
        var file = document.querySelector('.upload-file').files[0];
        var reader = new FileReader();

        reader.onloadend = function () {
            preview.src = reader.result;
        }

        if (file) {
            reader.readAsDataURL(file);
        } else {
            removeUpload();
        };
        $('.btn_close').on('click', function () {
            $(this).closest('.upload-file-card').removeClass('have-img');
            preview.src = '';
            $(this).closest('.upload-file-card').find('input').val('');
        });

    }
    $('.upload-file').on('change', function () {
        previewFile($(this)[0], $(this));
        $(this).closest('.upload-file-card').addClass('have-img');
    });


    // Add to favorite
    $(function () {
        $('#bookmarkme').click(function () {
            if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
                window.sidebar.addPanel(document.title, window.location.href, '');
            } else if (window.external && ('Добавить в избранное' in window.external)) { // IE Favorite
                window.external.AddFavorite(location.href, document.title);
            } else if (window.opera && window.print) { // Opera Hotlist
                this.title = document.title;
                return true;
            } else { // webkit - safari/chrome
                alert('Нажмите ' + (navigator.userAgent.toLowerCase().indexOf('mac') != -1 ? 'Command/Cmd' : 'CTRL') + ' + D чтобы добавить в избранное данный сайт.');
            }
        });
    });

});